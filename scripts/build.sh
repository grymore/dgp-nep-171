#!/bin/bash
set -e
cd ../"`dirname $0`"
source scripts/flags.sh
cargo build --all --target wasm32-unknown-unknown --release
cp target/wasm32-unknown-unknown/release/*.wasm ./res/


ID=nft-dgp.testnet 
near deploy --wasmFile res/non_fungible_token.wasm --accountId $ID
near call $ID new_default_meta '{"owner_id": "'$ID'"}' --accountId $ID
near view $ID nft_metadata                                                                             